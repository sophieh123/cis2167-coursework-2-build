<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Questionnaires</title>
</head>
<body>
<h1>Questionnaires</h1>
@can('see_adminnav')

    @include('admin/includes/adminnav') <!-- Displays admin nav bar if the user has this role and permission -->

@endcan

<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li> <!-- Displays list of questionnaire titles if at least one exists -->
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p> <!-- displays if no questionnaires have been created -->
    @endif
</section>
 <!-- Opens form to display button -->
{{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!} <!-- Displays button labelled "Create Questionnaire" -->
    </div>
{{ Form::close() }} <!-- Closes form -->

</body>
</html>
