<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  /*
    States that the attributes for role can be inputted through the system.
  */
  protected $fillable = [
      'name', 'label',
  ];

    public function permissions() {
      return $this->belongsToMany(Permission::class); // Letting the system know that roles belong to many permissions.
    }

    // Function to assign permissions to roles.
    public function givePermissionTo(Permission $permission) {
         return $this->permssion()->sync($permission);
     }
}
