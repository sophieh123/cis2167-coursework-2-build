<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Create Questionnaire</title>
</head>
<body>
<h1>Create Questionnaire</h1>

<!-- Opens create form for creating a questionnaire -->
{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!} <!-- Displays text box for attribute "title" -->
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Questionnaire Explanation and Ethics:') !!} <!-- Displays text box for attribute "content" -->
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Question', ['class' => 'button']) !!} <!-- Displays button labelled "Add Question" -->
    </div>
{!! Form::close() !!} <!-- Closes form -->

</body>
</html>
