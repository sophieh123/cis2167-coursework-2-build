<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new questionnaire');

// log in as your admin user
Auth::loginUsingId(1);

// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);


// add a test questionnaire to check that content can be seen in list at start

$I->haveRecord('questionnaires', [
    'id' => '9000',
    'title' => 'Questionnaire 1',
    'content' => 'questionnaire 1 content',
    'user_id' => 9999
]);


// Swap to new user so tests are created as this users id.
Auth::logout();
Auth::loginUsingId(9999);

// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaire', 'h1');
$I->see('questionnaire 1');
$I->dontSee('questionnaire 2');
// And
$I->click('Create Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires/create');
// And
$I->see('Create Questionnaire', 'h1');

$I->submitForm('#createquestionnaire', [
    'title' => 'Questionnaire 2',
    'content' => 'questionnaire 2 content',
]);

// how to handle the link table checking.

// check that the questionnaire has been written to the db then grab the new id ready to use as input to the link table.
$article = $I->grabRecord('questionnaires', ['title' => 'Questionnaire 2']);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');

$I->click('Questionnaire 2'); // the title is a link to the detail page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
$I->see('Questionnaire 2', 'h1');
$I->see('questionnaire 2 content');
$I->see('creator: testuser1'); 
