<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::auth(); // Applies authentication.

    Route::get('/home', 'HomeController@index'); // Links the homepage to the index of HomeController.

    Route::resource('/admin/questionnaires', 'QuestionnaireController' ); // Links the admins questionnaire page to the QuestionnaireController.

    Route::resource('/admin/users', 'UserController' ); // Links the admin user page to the UserController.

    Route::resource('/admin/questions', 'QuestionController' ); // Links the admin questions page to QuestionController.

    Route::resource('/admin/responses', 'ResponseController'); // Links the admin responses page to ResponseController.
});
