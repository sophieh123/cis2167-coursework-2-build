<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Role;

use Gate;

class UserController extends Controller
{

  /*
 * Secure the set of pages to the admin.
 */
  public function __construct()
  {
      $this->middleware('auth'); // Add authentications for the views linked to this controller.
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('see_all_users')) {
          $users = User::all();
          return view('admin/users/index', ['users' => $users]); // Can only view this page if a user has the "see_all_users" permission.
        }
        return view ('/home'); // Return to the homepage if the user does not have the correct permissions.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if (Gate::allows('see_all_users')) { // Can only edit a user if the "see_all_user" permission is assigned to the user.
        // get the user
        $user = User::where('id',$id)->first();
        $roles = Role::all();

        // if user does not exist return to list
        if(!$user)
        {
          return redirect('/admin/users');
        }
        return view('admin/users/edit')->with('user', $user)->with('roles', $roles); // Return the edit view of the user ID selected.
      }
      return view ('/home'); // Return back to the homepage if the user does not have the correct permissions.
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update function to allow different roles and information to be set to a user.
        $user = User::findOrFail($id);
        $roles = $request->get('role');

        $user->roles()->sync($roles);

        return redirect('/admin/users'); // Return back to the user
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
