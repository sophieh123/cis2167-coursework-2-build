<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Questionnaires</title>
  </head>
  <body>
    <h1>All Users</h1>
    @can('see_adminnav')

        @include('admin/includes/adminnav') <!-- Only shows the nav bar if the user has the correct permissions. -->

    @endcan

    <section>
     @if (isset ($users))

        <!-- Displays a table of lists of users with the headings "username", "email" and "permissions" -->
         <table>
             <tr>
                 <th>Username</th>
                 <th>Email</th>
                 <th>Permissions</th>
             </tr>
             @foreach ($users as $user)
                 <tr>
                     <td><a href="/admin/users/{{ $user->id }}/edit" name="{{ $user->name }}">{{ $user->name }}</a></td>
                     <td> {{ $user->email }}</td>
                     <td>
                       <ul>
                          @foreach($user->roles as $role)
                            <li>{{ $role->label }}</li>
                          @endforeach
                        </ul>
                     </td>
                 </tr>
             @endforeach
         </table>
     @else
         <p>no users</p> <!-- Displays if no users exist -->
     @endif
 </section>
  </body>
</html
