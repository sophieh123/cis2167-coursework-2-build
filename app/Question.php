<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  /*
    States that the attributes for question can be inputted through the system.
  */
  protected $fillable = [
    'question', 'questionnaire_id'
  ];

  public function questionnaire() {
    return $this->belongsTo(Questionnaire::class); // States that question belongs to questionnaire.
  }


}
