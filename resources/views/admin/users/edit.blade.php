<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Edit {{ $user->name }}</title>
</head>
<body>
<h1>Edit - {{ $user->name }}</h1>

<!-- Opens edit form for user -->
{!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}

<div>
    <!-- Displays label of "username" and a text box to edit -->
    {!! Form::label('name', 'Username:') !!}
    {!! Form::text('name', null) !!}
</div>

<div>
    <!-- Displays label of "Email Address" and a text box to edit -->
    {!! Form::label('email', 'Email Address:') !!}
    {!! Form::textarea('email', null) !!}
</div>

<div>
      <!-- Displays label of "Roles" and a check boxes to select and deselect -->
    {!! Form::label('roles', 'Roles:') !!}
    @foreach($roles as $role)
        {{ Form::label($role->name) }}
        {{ Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id]) }}
    @endforeach

</div>

<div>
    <!-- Displays a button labelled "update user and roles" -->
    {!! Form::submit('Update User and Roles') !!}
</div>


{!! Form::close() !!} <!-- Closes the form -->
</body>
</html>
