<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>Add Question</title>
</head>
<body>
<h1>Add Question</h1>

<!-- Opens create form for creating  a question -->
{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}
        {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question:') !!} <!-- Displays text box for attribute "question" -->
        {!! Form::text('question', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Response', ['class' => 'button']) !!} <!-- Displays button labelled "add response" -->
    </div>
{!! Form::close() !!} <!-- Closes form -->

</body>
</html>
