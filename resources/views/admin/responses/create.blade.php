<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Response</title>

    <link rel="stylesheet" href="/css/app.css" />

</head>
<body>
<h1>Create Response</h1>

<!-- Opens create form for creating a response -->
{!! Form::open(array('action' => 'ResponseController@store', 'id' => 'createresponse')) !!}

        {{ csrf_field() }}

    <div class="row large-12 columns">
        {!! Form::label('response', 'Response:') !!} <!-- Display text box for attribute "response" -->
        {!! Form::text('response', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Another Response', ['class' => 'button']) !!} <!-- Display button labelled "create another response" -->
    </div>
{!! Form::close() !!}

{{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}
    <div class="row large-4 columns">
        {!! Form::submit('Add New Question', ['class' => 'button']) !!} <!-- Displays button labelled "add new question" -->
    </div>
{{ Form::close() }}

{{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
    <div class="row large-4 columns">
        {!! Form::submit('Submit Questionnaires', ['class' => 'button']) !!} <!-- Displays button labelled "submit questionnaire" -->
    </div>
{{ Form::close() }}

</body>
</html>
