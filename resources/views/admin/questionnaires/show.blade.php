<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/app.css" />
    <title>{{ $questionnaire->title }}</title> <!-- Displays title of questionnaire in the tab -->
</head>
<body>

<h1>{{ $questionnaire->title }}</h1> <!-- Displays title of questionnaire -->
<p>{{ $questionnaire->content }}</p> <!-- Displays content of questionnaire -->
@foreach($questionnaire->questions as $question)
  <p>{{ $question->questions }}</p> <!-- Displays questions assigned to this questionnaire -->
@endforeach

<p>Created by: {{ Auth::user()->name }}</p> <!--Displays name of the creator of the questionnaire -->
</body>
</html>
