<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Questionnaire;

use App\Question;

class QuestionnaireController extends Controller
{

  /*
 * Secure the set of pages to the admin.
 */
    public function __construct()
    {
    $this->middleware('auth'); // Add authentications for the views linked to this controller.
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // get all the questionnaires
      $questionnaires = Questionnaire::all();

      return view('admin/questionnaires', ['questionnaires' => $questionnaires]); // Returns the view of the questionnaire blade.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/questionnaires/create'); // Returns the view of the questionnaires create form as this is where the create function will take place.


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $questionnaire = Questionnaire::create($request->all());
      $questionnaire->save();

      return view('admin/questions/create', ['questionnaire' => $questionnaire]); // Returns the view of the question create form to add to the questionnaire.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // get the questionnaire
      $questionnaire = Questionnaire::where('id',$id)->get()->first();

      $question = Question::where('questionnaire_id', $questionnaire->id)->get();
      $questionnaire['questions'] = $question;

      // if questionnaire does not exist return to list
      if(!$questionnaire)
      {
        return view('/admin/questionnaires');
      }
      return view('/admin/questionnaires/show')->withQuestionnaire($questionnaire);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
