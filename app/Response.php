<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
  /*
    States that the attributes for response can be inputted through the system.
  */
  protected $fillable = [
    'response',
  ];
}
