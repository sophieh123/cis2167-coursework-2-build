<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

          Model::unguard();

          //disable foreign key check for this connection before running seeders
          DB::statement('SET FOREIGN_KEY_CHECKS=0;');

          User::truncate();
          $this->call(UserTableSeeder::class);

          //re-enable foreign key check for this connection
          DB::statement('SET FOREIGN_KEY_CHECKS=1;');

          factory(User::class, 10)->create();

          Model::reguard();

    }
}
